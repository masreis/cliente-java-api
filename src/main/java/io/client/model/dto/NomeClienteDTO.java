package io.client.model.dto;

import javax.validation.constraints.NotBlank;

public class NomeClienteDTO {

    @NotBlank(message = "O nome é obrigatório")
    private String nome;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

}
