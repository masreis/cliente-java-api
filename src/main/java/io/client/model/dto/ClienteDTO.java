package io.client.model.dto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ClienteDTO {
    private Long id;
    @NotBlank(message = "O nome é obrigatório")
    private String nome;
    @NotBlank(message = "O cpf é obrigatório")
    private String cpf;
    @NotNull(message = "Data de nascimento é obrigatória")
    private LocalDate dataNascimento;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public long getIdadeEmAnos() {
        return ChronoUnit.YEARS.between(getDataNascimento(), LocalDate.now());
    }
}
