package io.client.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import io.client.exception.ClienteNotFoundException;
import io.client.model.Cliente;
import io.client.model.dto.ClienteDTO;
import io.client.model.dto.NomeClienteDTO;
import io.client.repository.ClienteRepository;
import io.client.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

    private ClienteRepository clienteRepository;
    private ModelMapper modelMapper;

    public ClienteServiceImpl(ClienteRepository clienteRepository, ModelMapper modelMapper) {
        this.clienteRepository = clienteRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public ClienteDTO save(ClienteDTO clienteDto) {
        Cliente saved = this.clienteRepository.save(convert(clienteDto));
        return convert(saved);
    }

    @Override
    public ClienteDTO update(Long id, ClienteDTO clienteDto) {
        if (!this.exist(id)) {
            throw new ClienteNotFoundException("Id not found: " + id);
        }
        Cliente cliente = convert(clienteDto);
        return convert(this.clienteRepository.save(cliente));
    }

    private ClienteDTO convert(Cliente cliente) {
        return this.modelMapper.map(cliente, ClienteDTO.class);
    }

    private Cliente convert(ClienteDTO clienteDto) {
        return this.modelMapper.map(clienteDto, Cliente.class);
    }

    @Override
    public Page<ClienteDTO> findAll(Pageable pg) {
        return this.clienteRepository.findAll(pg).map(this::convert);
    }

    @Override
    public ClienteDTO findByCpf(String cpf) {
        Cliente cliente = this.clienteRepository.findByCpf(cpf);
        if (cliente == null) {
            throw new ClienteNotFoundException("Cpf not found: " + cpf);
        } else {
            return convert(cliente);
        }
    }

    @Override
    public Page<ClienteDTO> findByNome(String nome, Pageable pg) {
        return this.clienteRepository.findByNomeContainsIgnoreCase(nome, pg).map(this::convert);
    }

    @Override
    public void delete(Long id) {
        this.clienteRepository.deleteById(id);
    }

    @Override
    public boolean exist(Long id) {
        return this.clienteRepository.existsById(id);
    }

    @Override
    public ClienteDTO findById(Long id) {
        return this.clienteRepository.findById(id).map(this::convert)
                .orElseThrow(() -> new ClienteNotFoundException("Id not found: " + id));
    }

    @Override
    public void updateNome(Long id, NomeClienteDTO nomeClienteDto) {
        ClienteDTO found = findById(id);
        found.setNome(nomeClienteDto.getNome());
        save(found);
    }
}
