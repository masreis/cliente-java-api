package io.client.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import io.client.model.dto.ClienteDTO;
import io.client.model.dto.NomeClienteDTO;

public interface ClienteService {

    ClienteDTO save(ClienteDTO clienteDto);

    ClienteDTO update(Long id, ClienteDTO clienteDto);

    Page<ClienteDTO> findAll(Pageable pg);

    ClienteDTO findByCpf(String cpf);

    Page<ClienteDTO> findByNome(String nome, Pageable page);

    void delete(Long id);

    boolean exist(Long id);

    ClienteDTO findById(Long id);

    void updateNome(Long id, NomeClienteDTO nomeClienteDto);

}