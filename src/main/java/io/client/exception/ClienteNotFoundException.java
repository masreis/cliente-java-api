package io.client.exception;

public class ClienteNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3825933546621500015L;

    public ClienteNotFoundException(String msg) {
        super(msg);
    }
}
