package io.client.exception;

import java.sql.SQLException;

import javax.validation.ValidationException;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ClienteExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { ClienteNotFoundException.class })
    protected ResponseEntity<Object> handleIllegalArgumentException(Exception exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getLocalizedMessage());
    }

    @ExceptionHandler({ SQLException.class, DataAccessException.class })
    protected ResponseEntity<Object> handleSqlException(Exception exception) {
        String msg = "Erro de integridade.";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
    }

    @ExceptionHandler(value = { ValidationException.class })
    protected ResponseEntity<Object> handleValidationException(Exception exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getLocalizedMessage());
    }

}
