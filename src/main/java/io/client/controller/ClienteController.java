package io.client.controller;

import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.client.exception.ClienteNotFoundException;
import io.client.model.dto.ClienteDTO;
import io.client.model.dto.NomeClienteDTO;
import io.client.service.ClienteService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    private ClienteService clienteService;
    @Value("${page.size}")
    private int pageSize;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @PostMapping
    @ApiOperation(value = "Rota para criar um cliente")
    public ResponseEntity<?> create(@Valid @RequestBody ClienteDTO clienteDto, BindingResult result,
            UriComponentsBuilder b) throws ValidationException {
        coletarErros(result);
        ClienteDTO saved = clienteService.save(clienteDto);
        UriComponents uriComponents = b.path("/clientes/{id}").buildAndExpand(saved.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Rota para atualizar um cliente")
    public ResponseEntity<ClienteDTO> update(@PathVariable final Long id, @Valid @RequestBody ClienteDTO clienteDto,
            BindingResult result) throws ValidationException {
        coletarErros(result);
        ClienteDTO updated = clienteService.update(id, clienteDto);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    private void coletarErros(BindingResult result) {
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(",")));
        }
    }

    @GetMapping
    @ApiOperation(value = "Rota para recuperar todos clientes")
    public ResponseEntity<Page<ClienteDTO>> findAll(@RequestParam(name = "page", defaultValue = "0") int page) {
        PageRequest pg = PageRequest.of(page, pageSize);
        Page<ClienteDTO> dtos = clienteService.findAll(pg);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Rota para recuperar pelo id")
    public ResponseEntity<ClienteDTO> findById(@PathVariable final Long id) {
        ClienteDTO clienteDto = clienteService.findById(id);
        return new ResponseEntity<>(clienteDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Rota para recuperar pelo cpf")
    @GetMapping("/findByCpf")
    public ResponseEntity<ClienteDTO> findByCpf(@RequestParam final String cpf) {
        ClienteDTO cliente = clienteService.findByCpf(cpf);
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @GetMapping("/findByNome")
    @ApiOperation(value = "Rota para recuperar pelo nome")
    public ResponseEntity<Page<ClienteDTO>> findByNome(@RequestParam final String nome,
            @RequestParam(name = "page", defaultValue = "0") int page) {
        PageRequest pg = PageRequest.of(page, pageSize);
        Page<ClienteDTO> dtos = clienteService.findByNome(nome, pg);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Rota para excluir um cliente")
    public ResponseEntity<Boolean> delete(@PathVariable final Long id) {
        if (clienteService.exist(id)) {
            clienteService.delete(id);
            return new ResponseEntity<>(Boolean.TRUE, HttpStatus.NO_CONTENT);
        } else {
            throw new ClienteNotFoundException("Id not found: " + id);
        }
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "Rota para atualizar o nome")
    public ResponseEntity<Boolean> patch(@PathVariable final Long id,
            @RequestBody final NomeClienteDTO nomeClienteDto) {
        if (clienteService.exist(id)) {
            clienteService.updateNome(id, nomeClienteDto);
            return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
        } else {
            throw new ClienteNotFoundException("Id not found: " + id);
        }
    }
}
