package io.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClienteJavaApiApp {
    public static void main(String[] args) {
        SpringApplication.run(ClienteJavaApiApp.class, args);
    }
}
