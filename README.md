# Client API

Aplicação cliente desenvolvida em Spring Boot com um CRUD para as operações:

* GET;
* POST;
* DELETE;
* PATCH;
* PUT.

Os endpoints podem ser conferidos na imagem abaixo.

![Endpoints](images/api-doc.png)

## Links

A interface do Swagger pode ser acessada na AWS no link <http://ec2-3-16-76-60.us-east-2.compute.amazonaws.com:9876/api/swagger-ui/#/>. O banco de dados é um RDS MySQL. A partir do Swagger é possível executar os endpoints com facilidade.

Para usar a aplicação localmente, o link para o Swagger é <http://localhost:8080/api/swagger-ui/#/>. Neste caso, é um MySQL local.

## Testes unitários

Não foram criados testes unitários, apenas os testes do Postman. Isto porque não foi solicitado na descrição do teste. Contudo, é recomendável ter ao menos 70% de cobertura de testes.

## Postman

Foram criados dois scripts do Postman para teste:

* `cliente-java-test-api.postman_collection.json`: aponta para o servidor `localhost`.
* `cliente-java-test-api.postman_collection-aws.json`: aponta para o servidor na AWS.
